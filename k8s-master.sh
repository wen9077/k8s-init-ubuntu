#!/bin/bash

sudo -s <<EOF
kubeadm init --service-cidr 10.96.0.0/12 --pod-network-cidr 10.244.0.0/16 --apiserver-advertise-address 0.0.0.0 && \
mkdir -p $HOME/.kube &&\
cp  /etc/kubernetes/admin.conf $HOME/.kube/config  &&\
chown $(id -u):$(id -g) $HOME/.kube/config &&\
kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml  
EOF
