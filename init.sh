#!/bin/bash


echo "init..." >> /tmp/init.log
sudo useradd ubuntu -m -s /bin/bash  2>/dev/null
sudo usermod -aG docker ubuntu
sudo su - root
echo "Install Docker ..." >> /tmp/init.log
sudo apt update && sudo apt install docker.io docker-compose -y

cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system

sudo apt-get install -y apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
echo "Install K8s" >> /tmp/init.log
sudo apt-get install -y  kubelet=1.22.0-00 kubeadm=1.22.0-00 kubectl=1.22.0-00

sudo systemctl enable --now kubelet



cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
sudo systemctl restart docker


sudo echo "DONE" >> /tmp/init.log


if [[ $HOSTNAME == "master" ]]
then
  sudo -s kubeadm init --service-cidr 10.96.0.0/12 --pod-network-cidr 10.244.0.0/16 --apiserver-advertise-address 0.0.0.0  && \
  sudo -s mkdir -p /root/.kube &&\
  sudo -s cp  /etc/kubernetes/admin.conf /root/.kube/config  &&\
  sudo -s chown $(id -u):$(id -g) /root/.kube/config &&\
  sudo -s kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml  &&\
  sudo -s kubeadm token create --print-join-command > /tmp/token &&\
  sudo -s mkdir -p /lab/ &&\
  sudo -s wget https://how64bit.com/tgc101-devops-demo/db.yml -P /lab &&\
  sudo -s wget https://how64bit.com/tgc101-devops-demo/php.yml -P /lab &&\
  sudo -s wget https://how64bit.com/tgc101-devops-demo/nginx.yml -P /lab 
  sudo -s kubectl apply -f /lab/.
fi





sudo echo "DONE" >> /tmp/init.log
